/*поиск маршрута*/

var metro = metro || { exit: {}};
metro.exit.router = metro.exit.router || {}

/**
 * Сервис построения маршрутов
 * @param config конфиг, содержащий данные неоходимые сервису. По началу можно инкапсулировать его внутрь класса.
 * Конфиг состоит из двух частей:
 *   1. Отображение gps координат на узлы. [{gps:node}]
 *   2. Список станций с их маршрутами [{station: {vestibule: [node]}}]
 *        node.parent
 *        node.children
 *        node.vestibule
 *        node.gps
 *        node.g.x
 *        node.g.y
 *        node.g.photo
 */
metro.exit.router.Router = function(config){

    var leafs = [];
    var gpsLeafs = [];
    var station;

    init();

    function init(){
        flatten(config.routes.root, config.routes.root.children)
        station = config
//        config - конфиг одной станции, пример leninProsp.json
//        преобразуем конфиг в дерево, flatten
//        во время преобразования все узлы с gps складываем в отдельный мап
    }

    function flatten(parent, children) {
//    function flatten(parent) {
        //var out = [];
        for (var i = 0; i < children.length; i++) {
            var node = children[i];
            node.parent = parent;
            //out.push(node);
            if (node.children != undefined && Array.isArray(node.children))
                flatten(node, node.children);
//                Array.prototype.push.apply(out, flatten(node.id, node.items));
//            delete node.items;
            else{
                leafs.push(node);
                gpsLeafs[metro.exit.gpskey(node.gps)] = node;
            }
        }
        //return out;
    }

    /**
     * @param gps координаты gps
     * @return node узел с координатами gps
     */
    function gpsToNode(gps){
    }

    /**
     * @param gps координаты gps
     * @return список найденных маршрутов для gps. В 90% это будет список из одного маршрута.
     *    маршрут это {вестибюль станции : [node.g]}.
     */
    this.routeGPS = function(gps){
        return routeNode(gpsLeafs[metro.exit.gpskey(gps)]);
    }

    /**
     * @param node лист, к которому будем строить маршрут
     * @return маршрут до найденного узла
     */
    this.route = function(node){
        for(var i = 0; i < leafs.length; i++)
            if(leafs[i].x == node.x && leafs[i].y == node.y){
                break;
            }
        //не проверяем, что leafs[i] существует, т.к. он должен существовать.
        return routeNode(leafs[i]);
    }

    this.map = function(){
        return station.map
    }

    this.exits = function(){
        return leafs
    }

    function routeNode(node){
        var result = [];
        while(node.parent !== undefined){
            //TODO json или другой формат приемлимый для drawer
            result.push(node);
            node = node.parent;
        }
        result.push(node);
        return result;
    }

}