/*поиск маршрута*/

var metro = metro || { exit: {}};
metro.exit.router = metro.exit.router || {}

//metro.exit.router = {}
/**
 * Сервис построения маршрутов
 */
metro.exit.router.Router = function() {
}

/**
 * @param stationId id станции
 * @param x см. Coord
 * @param y см. Coord
 * @param callback что исполнять, когда получим маршрут
 * @return список найденных маршрутов для gps. В 90% это будет список из одного маршрута.
 *    маршрут это {вестибюль станции : [node.g]}.
 */
metro.exit.router.Router.prototype.routeCoord = function(stationId, x, y, callback) {
    this.route({id: stationId, x: x, y: y}, callback)
}

/**
 * @param stationId id станции
 * @param lat см. GPS
 * @param lng см. GPS
 * @param callback что исполнять, когда получим маршрут
 * @return список найденных маршрутов для gps. В 90% это будет список из одного маршрута.
 *    маршрут это {вестибюль станции : [node.g]}.
 */
metro.exit.router.Router.prototype.routeGPS = function(stationId, lat, lng, callback) {
    this.route({id: stationId, lat: lat, lng: lng}, callback)
}

metro.exit.router.Router.prototype.route = function(params, callback) {
    if(params.lat == "55.708439")
        callback({"route":[{"x":900,"y":260,"photo":"stub"},{"x":1140,"y":260},{"x":1250,"y":180,"photo":"stub"}]})
    else if(params.lat == "55.706873"){
        callback({"route":[{"x":430,"y":260,"photo":"stub"},{"x":160,"y":260},{"x":160,"y":120},{"x":280,"y":120,"photo":"stub"}]})
    }else{
        alert("no stub for this gps.lat " + params.lat)
    }
//    $.ajax({
//        url: "route",
//        dataType: 'json',
//        data: params,
//        success:  function(data) {
//            callback(data)
//        },
//        error: function() {
//            alert("can't get route")
//        }
//    });
}

metro.exit.router.Router.prototype.station = function(stationId, callback) {
    if(stationId == "leninProsp")
        callback({map:"leninProsp",exits:[{x:1250,y:180,photo:"stub"},{x:280,y:120,photo:"stub"}]})
    else{
        alert("no stub for this station id: " + stationId)
    }
//    $.ajax({
//        url: "station",
//        dataType: 'json',
//        data: {id: stationId},
//        success: function(data) {
//            callback(data)
//        },
//        error: function() {
//            alert("can't get station")
//        }
//    });
}
