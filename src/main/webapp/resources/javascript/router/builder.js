var metro = metro || {exit:{}}
metro.exit.router = metro.exit.router || {}

metro.exit.router.build = function(stationId, lat, lng) {
    if(typeof(stationId) === 'string'){
        window.location.assign("router.html?id=" + stationId + "&lat=" + lat + "&lng=" + lng)
    }else{
        window.location.assign("selectRoute.html?" + stationId + "&lat=" + lat + "&lng=" + lng)
    }
}