/*получаем конфиг станции по имени*/
var metro = metro || { exit: {}};

metro.exit.config = function() {
    this.stationMap

    var config = this
    $.getJSON('stations', function(data) {
        $.each(data, function(key, val) {
            config.stationMap[key] = val;
        });
    });
}

metro.exit.config.prototype.getStationInfo = function(stationId) {
    return this.stationMap[stationId]
}