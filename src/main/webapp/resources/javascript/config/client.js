/*получаем конфиг станции по имени*/

var metro = metro || { exit: {}};

metro.exit.Config = function(){
}

////////////////////////////////////////////////////////////////////////
//////////////    берем иконку станции для карты      //////////////////
metro.exit.Config.prototype.getStationIcon = function(stationId){
    if(stationId === undefined)
        return "resources/images/icons/station/mix.png"
    else
        return "resources/images/icons/station/" + stationId + "/small.png"
}

metro.exit.Config.prototype.getStationMap = function(stationId){
    return "resources/images/scheme/" + stationId
}

metro.exit.Config.prototype.getStationName = function(stationId){
    return "Ленинский проспект"
}

metro.exit.Config.prototype.getPhoto = function(stationId, photo){
    return "resources/images/photo/" + stationId + "/" + photo + ".png"
}

metro.exit.Config.prototype.addressSeparator = "&"
metro.exit.Config.prototype.arraySeparator = "|"

metro.exit.config = new metro.exit.Config()

/*
 Загрузка конфига нужной станции
 function loadScript(url, callback)
 {
 // adding the script tag to the head as suggested before
 var head = document.getElementsByTagName('head')[0];
 var script = document.createElement('script');
 script.type = 'text/javascript';
 script.src = url;

 // then bind the event to the callback function
 // there are several events for cross browser compatibility
 script.onreadystatechange = callback;
 script.onload = callback;

 // fire the loading
 head.appendChild(script);
 }

 Then you write the code you want to use AFTER the script is loaded in a lambda function :

 var myPrettyCode = function() {

 // here, do what ever you want

 };

 Then you run all that :

 loadScript("my_lovely_script.js", myPrettyCode);


 OR jquery

 $.getScript("my_lovely_script.js", function(){


 alert("Script loaded and executed.");
 // here you can use anything you defined in the loaded script

 });

 */