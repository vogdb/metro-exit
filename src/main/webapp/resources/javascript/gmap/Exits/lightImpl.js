/////////////// выходы из метро, доступные для данной карты/////////////
//metro.exit.gmap = metro.exit.gmap || {}

metro.exit.gmap.Exits = function(map) {

    this.map = map//ссылка на google карту
    this.exits = new metro.exit.HashSet()//хранилище выходов
    this.markers = new metro.exit.gmap.Markers(map)//UI представление выходов

    this.init();
}
metro.exit.gmap.Exits.prototype.init = function() {
    var self = this
    google.maps.event.addListener(this.map, 'bounds_changed', function() {
        self.update(self.map.getBounds())
    });
}

/**
 * обновляет выходы из метро для заданных bounds
 * @param bounds
 */
metro.exit.gmap.Exits.prototype.update = function(bounds) {
    var self = this

    //обработчик полученных выходов
    var callback = function(data) {
        if (data.length !== undefined && data.length > 0 && data.length > self.exits.size()) {
            self.markers.drawMarkers(self.exits.putAll(data))
        }
    }

    //Обновление координат выходов
    if(bounds.getNorthEast().lat() - bounds.getSouthWest().lat() >= 1){
        return -1;
    }else{
        // ajax запрос здесь,получающий все координаты и id всех выходов из метро, лежащих в bounds
        // но пока stub на него
        callback([{lat: 55.708439, lng:37.587419, id: "leninProsp"}, {lat: 55.706873, lng:37.584946, id: ["leninProsp", "leninProsp"]}])
    }

}