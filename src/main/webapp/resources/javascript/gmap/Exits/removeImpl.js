/////////////// выходы из метро, доступные для данной карты/////////////
metro.exit.gmap = metro.exit.gmap || {}

metro.exit.gmap.Exits = function(map) {

    this.map = map
    this.exits = new metro.exit.HashSet()
    this.markers = new metro.exit.Markers(map)

    this.init();
}
metro.exit.gmap.Exits.prototype.init = function() {
    var self = this
    google.maps.event.addListener(this.map, 'bounds_changed', function() {
        self.update(self.map.getBounds())
    });
    //this.update(this.map.getBounds())
}
/**
 * обновляет выходы из метро для заданных bounds
 * @param bounds
 */
metro.exit.gmap.Exits.prototype.update = function(bounds) {
    var newExits = this.updater.update(bounds);
    if (newExits == 0) {
        this.map.setZoom(this.map.getZoom() - 1);
        return;
    }
    if (newExits !== -1) {
        if (newExits.length < this.exits.size()) {
            this.markers.removeMarkers(this.exits.retainAll(newExits));
        } else if (newExits.length > this.exits.size()) {
            this.markers.drawMarkers(this.exits.putAll(newExits));
        }
    } else {
        //Perfomance если включить, то учесть
        this.markers.removeMarkers(this.exits.content);
    }
}
////////////////////////////////////////////////////////////////////////