//описание package
var metro = metro || { exit: {}};
metro.exit.gmap = metro.exit.gmap || {}

/////////////// строит экземпляр карт. вызывается через callback metro.exit.gmap.load/////////////
metro.exit.gmap.build = function () {
    new metro.exit.gmap.builder("map_canvas");
}

/////////////// загружает библиотеку гугл карт для ключа параметра /////////////
metro.exit.gmap.load = function (key) {
    var script = document.createElement("script");
    script.type = "text/javascript";
    script.src = "http://maps.googleapis.com/maps/api/js?key=" + key + "&sensor=false&callback=metro.exit.gmap.build";
    document.body.appendChild(script);
}

/////////////// класс рисующий google карту с выходами метро/////////////
metro.exit.gmap.builder = function(mapCanvas) {
    this.canvas = document.getElementById(mapCanvas)//место для карты
    this.map//карта
    this.exits//выходы метро

    this.init();
}

metro.exit.gmap.builder.prototype.init = function() {
    var coord = this.parseInput()
    this.defineSize(this.canvas)
    this.map = this.createMap(coord)
    this.exits = new metro.exit.gmap.Exits(this.map)
}

/**
 * создает карту с центром в center
 * @param center
 */
metro.exit.gmap.builder.prototype.createMap = function(center) {
    var gcenter = new google.maps.LatLng(center.lat, center.lng)
    var myOptions = {
        zoom: 17,
        center: gcenter,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    };

    var map = new google.maps.Map(this.canvas, myOptions)

    var marker = new google.maps.Marker({

        position: gcenter,
        optimized: false,
        map: map,
        title:"Вам сюда"
    });

    return map
}

/**
 * определяем размер карты для клиента
 * @param mapdiv контейнер карты
 */
metro.exit.gmap.builder.prototype.defineSize = function(mapdiv) {
    var useragent = navigator.userAgent;

    if (useragent.indexOf('iPhone') != -1 || useragent.indexOf('Android') != -1) {
        mapdiv.style.width = '100%'
        mapdiv.style.height = '100%'
        mapdiv.style.padding = "0"
    } else {
        mapdiv.style.width = '95%'
        mapdiv.style.height = '100%'
    }
}

/**
 * выдираем коорд. из входящих GET параметров
 */
metro.exit.gmap.builder.prototype.parseInput = function() {
    var query = window.location.search.substring(1);
    var vars = query.split("&");
    if (vars.length !== 2)
        alert("неверные входящие параметры");

    return {lat: parseFloat(vars[0].split("=")[1]), lng: parseFloat(vars[1].split("=")[1])}
}
