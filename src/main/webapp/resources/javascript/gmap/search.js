//описание package
var metro = metro || { exit: {}};
metro.exit.gmap = metro.exit.gmap || {}

/**
 * находит заданный адрес
 * @param address
 * @param callback исполним после получения результата геокодирования
 */
metro.exit.gmap.search = function(address) {
    var geocoder = new google.maps.Geocoder();
    var request = {
        address: address,
        bounds: new google.maps.LatLngBounds(new google.maps.LatLng(55.0, 37.0), new google.maps.LatLng(56.0, 38.0))
    };
    geocoder.geocode(request, function(results, status) {
        if (results.length > 0 && status == google.maps.GeocoderStatus.OK) {
            results = filterResults(results)
            if (results.length > 0){
                if(results.length == 1){
                    window.location.assign("map.html?lat=" + results[0].geometry.location.lat()+"&lng=" + results[0].geometry.location.lng());
                }else{
                    window.location.assign("selectAddress.html?" + mergeAddresses(results));
                }
            }else{
                alert("Адрес находится за пределами Москвы");
            }
        } else {
            alert("Невозможно найти заданный адрес: " + status);
        }
    });

    function filterResults(results){
        var filtered = []
        for(var i = 0; i < results.length; i++){
            var location = results[i].geometry.location
            if(location.lat() > 55 && location.lat() < 56 && location.lng() > 37 && location.lng() < 38)
                filtered.push(results[i])
        }
        return filtered
    }

    function mergeAddresses(addresses){
        var merged = ""
        for(var i = 0; i < addresses.length - 1; i++){
            merged += formatAddress(addresses[i]) + metro.exit.config.addressSeparator
        }
        merged += formatAddress(addresses[i])
        return encodeURIComponent(merged)
    }

    function formatAddress(address){
        return address.formatted_address + metro.exit.config.arraySeparator
            + address.geometry.location.lat() + metro.exit.config.arraySeparator + address.geometry.location.lng()
    }
}