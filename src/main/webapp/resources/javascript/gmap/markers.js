////////////////////////////////////////////////////////////////////////
/////////////// Маркеры, обозначающие выходы из метро///////////////////
//metro.exit.gmap = metro.exit.gmap || {}

metro.exit.gmap.Markers = function(map) {
    this.map = map
/*
    this.icon = new google.maps.MarkerImage(
        "",
        new google.maps.Size(24,24),
        new google.maps.Point(0,0),
        new google.maps.Point(8,15),
        new google.maps.Point(24,24)
    )
*/
    this.markers = {}
}

metro.exit.gmap.Markers.prototype.removeMarkers = function (markers) {
    for (var key in markers)
        this.removeMarker(key)
}
metro.exit.gmap.Markers.prototype.removeMarker = function (key) {
    this.markers[key].setMap(null)
    delete this.markers[key]
}

metro.exit.gmap.Markers.prototype.drawMarkers = function (markers) {
    for (var key in markers)
        this.drawMarker(key, markers[key])
}
metro.exit.gmap.Markers.prototype.drawMarker = function (key, value) {
    var url
    if(typeof(value.id) === 'string'){
        url = metro.exit.config.getStationIcon(value.id)
    }else{
        url = metro.exit.config.getStationIcon()
    }
    var marker = new google.maps.Marker({

        position: new google.maps.LatLng(value.lat, value.lng),
        optimized: false,
        map: this.map,
        icon: new google.maps.MarkerImage(
            url,
            new google.maps.Size(24,24),
            new google.maps.Point(0,0),
            new google.maps.Point(8,15),
            new google.maps.Point(24,24)
        ),
        title:"Посмотреть маршрут до этого выхода"
    });

    google.maps.event.addListener(marker, 'click', function() {
        metro.exit.router.build(value.id, value.lat, value.lng)
    });

    this.markers[key] = marker
}