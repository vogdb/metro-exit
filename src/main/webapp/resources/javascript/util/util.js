/*полезности*/

var metro = metro || { exit: {}};

metro.exit.gpskey = function(gps){
//    return ((gps.lat.toFixed(6) * 100000000000000).toFixed(0) + (gps.lng.toFixed(6)*1000000).toFixed(0));
//    return gps.lat * 100000000000000 + gps.lng * 1000000;
    return gps.lat.toString() + gps.lng.toString();
}
metro.exit.trim = function (str) {
	var	str = str.replace(/^\s\s*/, ''),
		ws = /\s/,
		i = str.length;
	while (ws.test(str.charAt(--i)));
	return str.slice(0, i + 1);
}
metro.exit.nodekey = function (node) {
	return node.x * 10000 + node.y;
}
//////////// "100px" -> 100 ///////////////
metro.exit.getPx = function(param){
    return parseInt(param.split("px")[0])
}
