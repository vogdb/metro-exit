/*рисование полученных результатов*/

var metro = metro || { exit: {}};
metro.exit.drawer = metro.exit.drawer || {}

//canvas - где рисовать, на твое усмотрение, что сюда передавать div или альтернативу.
metro.exit.drawer.Drawer = function(canvas, router, stationId, callback) {

    this.canvas = document.getElementById(canvas)
    this.router = router
    this.stationId = stationId//ID отображаемой станции

    this.context//контекст где рисуем
    this.circle//константа обозначающая 2*Pi
    this.elems//содержит информацию по масштабированию
    this.route//отображаемый маршрут
    this.exits//отображаемые выходы станции
    this.photoIcon//иконка фотографии
    this.photo//окно для фотографий + данные о последней показанной фотографии

    this.touch//инфа о таче

    this.init(callback)
}

metro.exit.drawer.Drawer.prototype.init = function(callback) {
    //this.context = this.canvas.getContext("2d");
    this.circle = 2 * Math.PI
    this.elems = {}
    this.bg//текущая карта станции
    this.context = this.canvas.getContext("2d");

    this.photo = {}
    this.photo.popup = new metro.exit.popup("photo", 1000)
    this.photo.div = document.createElement("div")
    this.photo.div.style.width = "100%"
    this.photo.div.style.height = "100%"
    this.photo.popup.setContent(this.photo.div)

    this.photoIcon = new Image();
    this.photoIcon.src = "resources/images/icons/photo.png";
    this.photoIcon.onload = function() {

        callback()
    }

    var self = this
    this.canvas.addEventListener("touchstart", function(e) {
        self.touchstartListener(e)
    }, false)
    this.canvas.addEventListener("touchend", function(e) {
        self.clickListener(e)
    }, false)
    //TODO mouse click handlers, change route + redirect to new route exit on map
}

/**
 * рисует на канвасе карту станции
 * @param data данные для отрисовки.
 *  data.map гиперссылка на картинку станции.
 *  data.exits все узлы выходы со станции.
 * @param callback исполнить, когда выполнится загрузка карты.
 */
metro.exit.drawer.Drawer.prototype.setMap = function(data, callback) {
    this.elems.node = {}
    this.elems.node.radius = 17

    this.elems.photoIcon = {}
    this.elems.photoIcon.width = this.photoIcon.naturalWidth
    this.elems.photoIcon.height = this.photoIcon.naturalHeight

    this.elems.line = 5

    var self = this
    var bg = new Image();
    bg.src = "resources/images/scheme/" + data.map + ".png";
    this.bg = bg

    bg.onload = function() {
        self.drawBG()
        //TODO в другом месте, отдельный метод
        self.setExits(data.exits)
        callback()
    };
}

metro.exit.drawer.Drawer.prototype.drawBG = function() {
    this.canvas.style.background = "no-repeat url(" + this.bg.src+ ")"
    this.canvas.width = this.bg.naturalWidth
    this.canvas.height = this.bg.naturalHeight
}

/**
 * рисует на канвасе
 * @param exits массив узлов выходов из станции
 *  структура node
 *    node.x - x координата узла на карте
 *    node.y - y координата узла на карте
 *    node.photo - гиперссылка на фото узла
 */
metro.exit.drawer.Drawer.prototype.setExits = function(exits) {
    this.context.beginPath()
    for (var i = 0; i < exits.length; i++) {
        this.drawNode(exits[i])
    }
    this.context.fillStyle = "green"
    this.context.fill()
    this.exits = exits
    //this.context.closePath()
}

/**
 * рисует на канвасе заданный маршрут
 * @param route маршрут
 *  route массив node, где node узел маршрута
 *  структура node
 *    node.x - x координата узла на карте
 *    node.y - y координата узла на карте
 *    node.photo - гиперссылка на фото узла
 *  обращение к опр-ой node = route[i]
 */
metro.exit.drawer.Drawer.prototype.setRoute = function(route) {
//    if (this.route) {
//        this.drawBG()
//    }
    this.context.beginPath()
    this.route = route
    //чертим
    for (var i = 0; i < route.length - 1; i++) {
        this.drawEdge(route[i], route[i + 1])
        this.drawNode(route[i])
    }
    this.drawNode(route[i])
    //заливаем и красим
    this.context.strokeStyle = this.context.fillStyle = "red"
    this.context.lineWidth = this.elems.line
    this.context.fill()
    this.context.stroke()
    //this.context.closePath()
}

metro.exit.drawer.Drawer.prototype.drawEdge = function(from, to) {
    this.context.moveTo(from.x, from.y)
    this.context.lineTo(to.x, to.y)
}

metro.exit.drawer.Drawer.prototype.drawNode = function(node) {
    this.context.moveTo(node.x, node.y)
    this.context.arc(
        node.x,
        node.y,
        this.elems.node.radius,
        0, this.circle, true);
    if (node.photo) {
        this.drawPhoto(node)
    }
}

metro.exit.drawer.Drawer.prototype.drawPhoto = function(node) {

    this.context.drawImage(this.photoIcon,
        node.x - this.elems.node.radius,
        node.y - this.elems.node.radius - this.elems.photoIcon.height,
        this.elems.photoIcon.width,
        this.elems.photoIcon.height
    )

}

metro.exit.drawer.Drawer.prototype.touchstartListener = function(e) {
    if (e.touches.length == 1) {
        this.touch = e.touches[0];
    }
}

metro.exit.drawer.Drawer.prototype.clickListener = function(e) {
    var touches
    if (e.changedTouches === undefined)
        touches = e.touches
    else
        touches = e.changedTouches
    if (touches !== undefined && touches.length == 1 && touches[0].pageX == this.touch.pageX && touches[0].pageY == this.touch.pageY) {
        if (this.exits && this.route) {
            var event = touches[0]
            var coord = this.getCursorPosition(event);
            //var coord2 = this.getCursorPosition2(event);
            //alert('{' + event.pageX + ':' + event.pageY + '}' + '{' + coord.x + ':' + coord.y + '}')
            //alert('{' + coord.x + ':' + coord.y + '}' + '{' + coord2.x + ':' + coord2.y + '}')
            for (var i = 0; i < this.exits.length; i++) {
                if (this.intersect(this.exits[i], coord)) {
                    this.popupPhoto(this.exits[i].photo)
                    return
                }
            }
            for (var i = 0; i < this.route.length; i++) {
                if (this.route[i].photo && this.intersect(this.route[i], coord))
                    this.popupPhoto(this.route[i].photo)
            }
        }
    }
}

metro.exit.drawer.Drawer.prototype.popupPhoto = function(photo) {
    //window.location.assign(metro.exit.config.getPhoto(this.stationId, photo))
    window.location.href = metro.exit.config.getPhoto(this.stationId, photo)
    //window.location.href = "touch.html"

//    var photoUrl = this.stationId + "/" + photo
//    if (!this.photo.current || this.photo.current != photoUrl) {
//        this.photo.current = photoUrl
//        this.photo.div.style.backgroundImage = "url(" + metro.exit.config.getPhoto(this.stationId, photo) + ")"
//    }
//    this.photo.popup.show()
}

metro.exit.drawer.Drawer.prototype.getCursorPosition = function(e) {
    var x;
    var y;
    if (e.pageX != undefined && e.pageY != undefined) {
        x = e.pageX;
        y = e.pageY;
    }
    else {
        x = e.clientX + document.body.scrollLeft +
            document.documentElement.scrollLeft;
        y = e.clientY + document.body.scrollTop +
            document.documentElement.scrollTop;
    }
    x -= (this.canvas.offsetLeft) ? this.canvas.offsetLeft : 0;
    y -= (this.canvas.offsetTop) ? this.canvas.offsetTop : 0;
    return {x: x, y: y}
}

metro.exit.drawer.Drawer.prototype.getCursorPosition2 = function(e) {
    var element = this.canvas, offsetX = 0, offsetY = 0, mx, my;

    // Compute the total offset
    if (element.offsetParent !== undefined) {
        do {
            offsetX += element.offsetLeft;
            offsetY += element.offsetTop;
        } while ((element = element.offsetParent));
    }

    // Add padding and border style widths to offset
    // Also add the <html> offsets in case there's a position:fixed bar
    offsetX += (this.stylePaddingLeft) ? this.stylePaddingLeft : 0 + (this.styleBorderLeft) ? this.styleBorderLeft : 0 + (this.htmlLeft) ? this.htmlLeft : 0;
    offsetY += (this.stylePaddingTop) ? this.stylePaddingTop : 0 + (this.styleBorderTop) ? this.styleBorderTop : 0 + (this.htmlTop) ? this.htmlTop : 0;

    mx = e.pageX - offsetX;
    my = e.pageY - offsetY;

    // We return a simple javascript object (a hash) with x and y defined
    return {x: mx, y: my};
}

metro.exit.drawer.Drawer.prototype.intersect = function(node, coord) {

    var photoIconX = node.x - this.elems.node.radius
    var photoIconY = node.y - this.elems.node.radius - this.elems.photoIcon.height

    return (coord.x >= photoIconX && coord.x <= photoIconX + this.elems.photoIcon.width) &&
        (coord.y >= photoIconY && coord.y <= photoIconY + this.elems.photoIcon.height)
}
