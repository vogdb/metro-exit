window.onload = initialize;

function initialize() {

    function search(address, prefix) {
        metro.exit.gmap.search(prefix + address)
    }

    document.getElementById("address").onfocus = function() {
        if (this.className == "hint") {
            this.className = ""
            this.value = ""
        }
    }

    document.getElementById("go").onclick = function() {
        var input = document.getElementById("address");
        if (input.className !=="hint" && metro.exit.trim(input.value) !== undefined && metro.exit.trim(input.value) !== "") {
            search(input.value, "Россия, Москва, ");
        } else {
            var select = document.getElementById("station");
            if (select.selectedIndex > 0)
                search(select.value, "Россия, Москва, Метро ");
            else
                alert("выберите станцию метро или введите адрес");
        }
        return false;
    }
}