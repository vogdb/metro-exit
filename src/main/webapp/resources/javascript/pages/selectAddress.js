window.onload = initialize;

function initialize() {

    //вырезаем из запроса "?", поэтому substring(1)
    var query = decodeURIComponent(window.location.search.substring(1))
    var addresses = query.split(metro.exit.config.addressSeparator)
    if (addresses.length <= 1)
        alert("неверные входящие параметры")

    var container = document.getElementById("addresses")
    for(var i = 0; i < addresses.length; i++){
        var addressInput = createAddressInput(addresses[i])
        container.appendChild(addressInput[0])
        container.appendChild(addressInput[1])
    }

    document.getElementById("go").onclick = function() {
        var radios = document.getElementsByName("addresses");
        var input = undefined
        for(var i = 0; i < radios.length; i++)
            if(radios[i].checked){
                input = radios[i].value
                break
            }
        if (input !== undefined) {
            window.location.assign("map.html?" + input);
        } else {
            alert("выберите адрес");
        }
        return false;
    }

    function createAddressInput(address){
        var input = document.createElement("input")
        input.setAttribute("type", "radio");
        input.setAttribute("name", "addresses");

        var addressParts = address.split(metro.exit.config.arraySeparator)
        //input.value = "lat=" + addressParts[1] + "&lng=" + addressParts[2]
        input.setAttribute("value", "lat=" + addressParts[1] + "&lng=" + addressParts[2]);
        var title = document.createElement("span")
        title.innerHTML = addressParts[0] + "<br>"
        return [input, title]
    }
}