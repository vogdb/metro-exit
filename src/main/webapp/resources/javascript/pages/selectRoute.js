window.onload = initialize;

function initialize() {

    //вырезаем из запроса "?", поэтому substring(1)
    var query = decodeURIComponent(window.location.search.substring(1))
    var parts = query.split("&")
    if (parts.length !== 3)
        alert("неверные входящие параметры")

    var container = document.getElementById("routes")
    var stations = parts[0].split(",")
    for(var i = 0; i < stations.length; i++){
        var stationInput = createStationInput(stations[i])
        container.appendChild(stationInput[0])
        container.appendChild(stationInput[1])
    }

    document.getElementById("go").onclick = function() {
        var radios = document.getElementsByName("stations");
        var input = undefined
        for(var i = 0; i < radios.length; i++)
            if(radios[i].checked){
                input = radios[i].value
                break
            }
        if (input !== undefined) {
            window.location.assign("router.html?id=" + input + "&" + parts[1] + "&" + parts[2]);
        } else {
            alert("выберите станцию");
        }
        return false;
    }

    function createStationInput(stationId){
        var input = document.createElement("input")
        input.setAttribute("type", "radio");
        input.setAttribute("name", "stations");

        input.setAttribute("value", stationId);
        var title = document.createElement("span")
        title.innerHTML = metro.exit.config.getStationName(stationId) + "<br>"
        return [input, title]
    }
}