window.onload = initialize;

function initialize() {


    var params = metro.exit.parseRoute()
    var router = new metro.exit.router.Router()

    var callback = function(){
        router.station(params[0], function(data) {
            drawer.setMap(data, function() {
                router.routeGPS(params[0], params[1], params[2], function(data) {
                    drawer.setRoute(data.route)
                })
            })
        })
    }

    var drawer = new metro.exit.drawer.Drawer("container", router, params[0], callback)
}
/**
 * выдираем id и gps из входящих GET параметров
 */
metro.exit.parseRoute = function() {
    var query = window.location.search.substring(1);
    var vars = query.split("&");
    if (vars.length > 3)
        throw "неверные входящие параметры";

    return [ vars[0].split("=")[1], vars[1].split("=")[1], vars[2].split("=")[1]]
}
