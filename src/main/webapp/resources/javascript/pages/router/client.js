window.onload=initialize;

function initialize() {

    //TODO заменить station на динамическую загрузку по stationId
    //считаем, что leninProsp.json уже загружен.
    var router = new metro.exit.router(station)
    var drawer = new metro.exit.drawer("canvas", router)

    drawer.setMap(router.map())
    drawer.setExits(router.exits())
    drawer.setRoute(router.routeGPS({ lat: 55.708443, lng:37.587445 }))
}