package com.garageteam.metroexit;

import org.apache.log4j.Logger;
import org.codehaus.jackson.JsonFactory;
import org.codehaus.jackson.JsonGenerator;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author Sanin Alex Date: 1/14/12 Time: 2:00 PM
 */
public class RouterServlet extends HttpServlet {

    private final static Logger log = Logger.getLogger(RouterServlet.class);

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        log.debug("getting route");
        String stationId = req.getParameter("id");
        String lat = req.getParameter("lat");
        String lng = req.getParameter("lng");
        String x = req.getParameter("x");
        String y = req.getParameter("y");
        if (log.isDebugEnabled())
            log.debug("stationId:[" + stationId + "] lat:[" + lat + "] lng:[" + lng + "]");
        if (req.getMethod().equals("GET") && stationId != null && stationId.length() < 30
                && ((lat != null && lat.matches(GPS.PATTERN) && lng != null && lng.matches(GPS.PATTERN))
                || (x != null && x.matches(Coord.PATTERN) && y != null && y.matches(Coord.PATTERN)))
                ) {
            try {
                JsonFactory jsonFactory = JsonFactoryHolder.getFactory();
                Config config = Config.getInstance();
                Station station = config.getStation(stationId);
                resp.setContentType("application/json");
                JsonGenerator generator = jsonFactory.createJsonGenerator(resp.getOutputStream());
                Router router = Router.getInstance();

                //начало ответа
                generator.writeStartObject();
                //маршрут для запрошенных координат
                if (lat != null)
                    router.writeRouteGPS(new GPS(lat, lng), station, generator);
                else
                    router.writeRouteCoord(new Coord(Integer.valueOf(x), Integer.valueOf(y)), station, generator);
                generator.close();
            } catch (NoStationException e) {
                log.error("", e);
            } catch (Exception e) {
                log.error("unexpected error", e);
            }
        } else {
            log.warn("trying get route with wrong parameters");
        }
    }
}
