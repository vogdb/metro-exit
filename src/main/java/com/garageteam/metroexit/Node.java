package com.garageteam.metroexit;

import java.util.List;

/**
 * @author Sanin Alex Date: 1/14/12 Time: 21:15
 */
public class Node {
    Node parent;
    List<Node> children;
    Coord coord;
    List<Coord> pre;
    String photo;
    GPS gps;

    @Override
    public String toString() {
        return "Node{" +
                "parent=" + ((parent != null) ? parent.coord : null) +
                ", children=" + ((children != null) ? children : null) +
                ", coord=" + ((coord != null) ? coord : null) +
                ", pre=" + ((pre != null) ? pre : null) +
                ", photo='" + ((photo != null) ? photo : null) + '\'' +
                ", gps=" + ((gps != null) ? gps : null) +
                '}';
    }
}
