package com.garageteam.metroexit;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * @author Sanin Alex Date: 1/14/12 Time: 2:00 PM
 */
public class ExitsUpdaterServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        ExitsUpdater updater = ExitsUpdater.getInstance();
        List<GPS> result = updater.update(1.0, 2.0, 1.0, 2.0);
    }
}
