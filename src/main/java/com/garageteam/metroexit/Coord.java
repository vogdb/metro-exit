package com.garageteam.metroexit;

/**
 * @author Sanin Alex Date: 1/16/12 Time: 20:51
 */
public class Coord {
    public static final String PATTERN = "^[\\d]{1,5}$";

    public int x;
    public int y;
    public String photo;

    public Coord() {
    }

    public Coord(int x, int y) {
        this.x = x;
        this.y = y;
    }

    @Override
    public String toString() {
        return "Coord{" +
                "x=" + x +
                ", y=" + y +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        return (this == o) || ((o instanceof Coord) && hashCode() == o.hashCode());
    }

    @Override
    public int hashCode() {
        return x << 4 + y;
    }
}
