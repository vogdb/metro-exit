package com.garageteam.metroexit;

/**
 * @author Sanin Alex Date: 1/14/12 Time: 21:24
 */
public class NoStationException extends Exception {
    public NoStationException() {
        super();
    }
}
