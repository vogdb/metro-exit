package com.garageteam.metroexit;

import org.codehaus.jackson.JsonFactory;

/**
 * @author Sanin Alex Date: 1/17/12 Time: 23:20
 */
public class JsonFactoryHolder {

    private static JsonFactory factory;

    static {
        factory = new JsonFactory();
    }

    public static JsonFactory getFactory() {
        return factory;
    }
}
