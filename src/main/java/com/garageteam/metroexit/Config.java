package com.garageteam.metroexit;

import org.apache.log4j.Logger;
import org.codehaus.jackson.JsonFactory;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.JsonParser;
import org.codehaus.jackson.map.MappingJsonFactory;

import java.io.File;
import java.util.*;

/**
 * @author Sanin Alex Date: 1/15/12 Time: 23:21
 */
public class Config {

    private final static Logger log = Logger.getLogger(Config.class);

    private static Config config;

    Map<String, Station> stations = new HashMap<String, Station>();

    public Station getStation(String id) throws NoStationException {
        if (stations.containsKey(id))
            return stations.get(id);
        else
            throw new NoStationException();
    }

    public static Config getInstance() {
        return config;
    }

    static {
        config = new Config();
    }

    private Config() {
//        JsonFactory jsonFactory = new JsonFactory();
        JsonFactory jsonFactory = new MappingJsonFactory();
        File configDir = new File("/home/vogdb/workspace/metro-exit/trunk/src/main/webapp/resources/javascript/config/stations");
        for (File sConfig : configDir.listFiles()) {
            try {
                if(log.isDebugEnabled())
                    log.debug("parsing config: " + sConfig.getName());
                String stationId;
                Station station = new Station();
                JsonParser parser = jsonFactory.createJsonParser(sConfig);
                parser.nextToken();

                parser.nextToken();
                parser.nextToken();
                stationId = station.id = parser.getText();

                parser.nextToken();
                parser.nextToken();
                station.name = parser.getText();

                parser.nextToken();
                parser.nextToken();
                station.map = parser.getText();

                //маршруты
                parser.nextToken();
                parser.nextToken();
                parser.nextToken();
                //берем значение узла root
                parser.nextToken();

                station.root = readNode(parser.readValueAsTree(), null, station);

                //parser.nextToken();
                //parser.nextToken();

                //узел с координатами корня маршрутов
                //List<Integer> l = parser.readValueAs(new TypeReference<List<Integer>>() {});

                if(log.isDebugEnabled())
                    log.debug("parsed config: " + station);
                stations.put(stationId, station);
            } catch (Exception e) {
                log.error("can't parse " + sConfig.getName(), e);
            }
        }
    }

    private Node readNode(JsonNode jsonNode, Node parent, Station station){
        Node result = new Node();
        result.parent = parent;
        result.coord = readCoord(jsonNode.get("coord"));
        if(jsonNode.has("pre")){
            result.pre = readPre(jsonNode);
        }
        if(jsonNode.has("gps")){
            result.gps = readGps(jsonNode);
        }
        if(jsonNode.has("children")){
            Iterator<JsonNode> it = jsonNode.get("children").iterator();
            result.children = new LinkedList<Node>();
            while(it.hasNext()){
                result.children.add(readNode(it.next(), result, station));
            }
        }else{
            station.coordLeafs.put(result.coord, result);
            station.gpsLeafs.put(result.gps, result);
        }
        return result;
    }

    private GPS readGps(JsonNode jsonNode) {
        GPS result = new GPS();
        result.lat = jsonNode.get("gps").get("lat").asDouble();
        result.lng = jsonNode.get("gps").get("lng").asDouble();
        return result;
    }

    private List<Coord> readPre(JsonNode jsonNode) {
        List<Coord> result = new LinkedList<Coord>();
        Iterator<JsonNode> it = jsonNode.get("pre").getElements();
        while(it.hasNext())
            result.add(readCoord(it.next()));
        return result;
    }

    private Coord readCoord(JsonNode jsonNode){
        Iterator<JsonNode> it = jsonNode.getElements();
        Coord result = new Coord();
        result.x = it.next().asInt();
        result.y = it.next().asInt();
        if(jsonNode.has("photo")){
            result.photo = jsonNode.get("photo").getTextValue();
        }
        return result;
    }

    public static void main(String [] args){

    }
}
