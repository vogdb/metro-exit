package com.garageteam.metroexit;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Sanin Alex Date: 1/14/12 Time: 21:20
 */
public class Station {
    public String id;
    public String name;
    public String map;
    public Map<GPS, Node> gpsLeafs;
    public Map<Coord, Node> coordLeafs;
    public Node root;

    public Station() {
        gpsLeafs = new HashMap<GPS, Node>();
        coordLeafs = new HashMap<Coord, Node>();
    }

    @Override
    public String toString() {
        return "Station{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", map='" + map + '\'' +
                ", coordLeafs=" + coordLeafs +
                ", gpsLeafs=" + gpsLeafs +
                ", root=" + root +
                '}';
    }
}
