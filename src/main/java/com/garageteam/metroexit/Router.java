package com.garageteam.metroexit;

import org.codehaus.jackson.JsonGenerator;

import java.io.IOException;
import java.util.Collection;

/**
 * @author Sanin Alex Date: 1/14/12 Time: 2:00 PM
 */
public class Router {

    private static Router instance = new Router();

    public static Router getInstance() {
        return instance;
    }

    private Config config = Config.getInstance();

    public String getStationImg(String stationId) throws NoStationException {
        return config.getStation(stationId).map;
    }

    public Collection<Node> getStationExits(String stationId) throws NoStationException {
        return config.getStation(stationId).gpsLeafs.values();
    }

    public void writeRouteGPS(GPS gps, Station station, JsonGenerator generator) throws IOException {
        writeRoute(station.gpsLeafs.get(gps), generator);
    }

    public void writeRouteCoord(Coord coord, Station station, JsonGenerator generator) throws IOException {
        writeRoute(station.coordLeafs.get(coord), generator);
    }

    private void writeRoute(Node node, JsonGenerator generator) throws IOException {
        generator.writeArrayFieldStart("route");

        do {
            writeRouteNode(node, generator);
            node = node.parent;
        } while (node.parent != null);

        generator.writeEndArray();

    }

    public void writeNode(Node node, JsonGenerator generator) throws IOException {
        generator.writeStartObject();
        writeCoord(node.coord, generator);
        generator.writeEndObject();

    }

    public void writeRouteNode(Node node, JsonGenerator generator) throws IOException {
        if (node.pre != null)
            for (Coord coord : node.pre) {
                generator.writeStartObject();
                writeCoord(coord, generator);
                generator.writeEndObject();
            }
        writeNode(node, generator);
    }

    private void writeCoord(Coord coord, JsonGenerator generator) throws IOException {
        generator.writeNumberField("x", coord.x);
        generator.writeNumberField("y", coord.y);
        if(coord.photo != null)
            generator.writeStringField("photo", coord.photo);
    }
}
