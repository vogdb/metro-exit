package com.garageteam.metroexit;

import org.apache.log4j.Logger;
import org.codehaus.jackson.JsonFactory;
import org.codehaus.jackson.JsonGenerator;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author Sanin Alex Date: 1/15/12 Time: 2:46 PM
 */
public class StationServlet extends HttpServlet {

    private final static Logger log = Logger.getLogger(StationServlet.class);

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        log.debug("getting station");
        String stationId = req.getParameter("id");
        if(log.isDebugEnabled())
            log.debug("stationId:[" + stationId + "]");
        if (req.getMethod().equals("GET") && stationId != null && stationId.length() < 30) {
            try {
                JsonFactory jsonFactory = JsonFactoryHolder.getFactory();
                Config config = Config.getInstance();
                Station station = config.getStation(stationId);
                resp.setContentType("application/json");
                JsonGenerator generator = jsonFactory.createJsonGenerator(resp.getOutputStream());
                //начало ответа
                generator.writeStartObject();

                generator.writeStringField("map", station.map);
                //все выходы станции
                generator.writeArrayFieldStart("exits");
                Router router = Router.getInstance();
                for (Node node : station.gpsLeafs.values()) {
                    router.writeNode(node, generator);
                }
                generator.writeEndArray();
                //окончание ответа
                generator.writeEndObject();
                generator.close();
                //resp.flushBuffer();
                log.debug("station succesfully defined");
            } catch (Throwable t) {
                resp.reset();
                resp.sendError(404, "**** you");
                log.error("error getting station", t);
            }
        } else {
            log.error("getting station. bat request params");
        }

    }

    private void writeJson(JsonGenerator generator, Node node) {

    }
}
