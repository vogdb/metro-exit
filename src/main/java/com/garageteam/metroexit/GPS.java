package com.garageteam.metroexit;

/**
 * @author Sanin Alex Date: 1/14/12 Time: 2:10 PM
 */
public class GPS {

    public double lat;
    public double lng;
    public static final String PATTERN = "^[\\d]{2}\\.[\\d]{4,6}$";

    public GPS() {
    }

    public GPS(String lat, String lng) {
        this.lat = Double.valueOf(lat);
        this.lng = Double.valueOf(lng);
    }

    @Override
    public String toString() {
        return "GPS{" +
                "lat=" + lat +
                ", lng=" + lng +
                '}';
    }

    @Override
    public int hashCode() {
        return (int)(Double.doubleToRawLongBits(lat) / 17 * 100000 + Double.doubleToRawLongBits(lng));
    }

    @Override
    public boolean equals(Object obj) {
        return obj instanceof GPS && obj.hashCode() == hashCode();
    }
}
