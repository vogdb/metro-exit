clear

usage(){
	echo "Usage: $0 directory"
	exit 1
}

[[ $# -eq 0 ]] && usage

directory(){
    local FILE_NAME
    local FILE_EXT
    for f in $1/*
    do
        FILE_NAME=$(basename $f)
        FILE_EXT=${FILE_NAME##*.}
        if [ "$FILE_EXT" = "png" -o "$FILE_EXT" = "jpg" ]
        then
            echo "Composing $f file.."
            #OUTPUT_FILE="composed.`basename $f`"
            convert -size 140x80 xc:none -fill grey -gravity NorthWest -draw "text 10,10 'metroexit'" -gravity SouthEast -draw "text 5,15 'sanin alex vogdb'" miff:- | composite -tile - $f $f;
        elif [ -d "$f" ];then
            directory $f;
        fi
    done
}

directory $1